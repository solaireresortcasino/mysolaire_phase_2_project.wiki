<b>Design Document: </b>[Solaire_Mobile_SPSite__Technical_Design_Document_v1.4.docx](/uploads/13cef8aa8d0f2778dac90486c452ea6e/Solaire_Mobile_SPSite__Technical_Design_Document_v1.4.docx)

<b>Deployment Guide :</b>[SharePoint_Deployment_Guide_V_1.1.docx](/uploads/20a3bc21d588313106d6a5c1dffcd5a1/SharePoint_Deployment_Guide_V_1.1.docx)

<b>Database backup steps:</b>[DatabaseBackupSteps.docx](/uploads/04ca29895377a4acf01f67614bc31ba8/DatabaseBackupSteps.docx)

<b>Production migration steps:</b>[ProdMigration.docx](/uploads/7b837a940034ed88e05eaf139e0c5330/ProdMigration.docx)

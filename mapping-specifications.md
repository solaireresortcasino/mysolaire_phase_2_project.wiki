**ESB Mapping Specs**

*  **Search Room** - [Search_Room_v1.2.xlsx](/uploads/32aa3f06bdd019e8f627a39d84f6ff8a/Search_Room_v1.2.xlsx)
*  **Search Packages** - [Search_Packages_v1.2.xlsx](/uploads/5acf72653428907fcea6700216f4e160/Search_Packages_v1.2.xlsx)
*  **Detailed Availability**- [Detailed_Availability_v1.3.xlsx](/uploads/dec77700ca779528773a5089f3c4584c/Detailed_Availability_v1.3.xlsx)
* **Cancel Booking** - [Cancel_Booking_v1_3.xlsx](/uploads/8682c26fb038cbfde51e6f0e4eb6f0e6/Cancel_Booking_v1_3.xlsx)
*  **Payment Authorization**[PaymentAuthorization_1.5.xlsx](/uploads/3a7bb889b522d6653de5a001622b7620/PaymentAuthorization_1.5.xlsx)
*  **Redeem Points** [RedeemPoints_v1.2.xlsx](/uploads/c3274c644474b1cbf09d67640df5290e/RedeemPoints_v1.2.xlsx)
* **Update Registration** [UpdateRegistration_v1.1.xlsx](/uploads/3ad6dcca9dbd824abb4db78688e9e8f1/UpdateRegistration_v1.1.xlsx) 
*  **Express CheckOut**[ExpressCheckOut_v1.2.xlsx](/uploads/ffabeff3e371362b9db2381d486b4ef9/ExpressCheckOut_v1.2.xlsx)
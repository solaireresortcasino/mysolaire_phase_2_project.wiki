<b>MySolaire mobile app Phase 2 - BRD :</b> [Solaire_Mobile_App_Phase_2_-_BRD_v1.9.doc](/uploads/9da7d6540e5b2e9cdc8e4d4e345c98fd/Solaire_Mobile_App_Phase_2_-_BRD_v1.9.doc)

<b>Design document :</b>[Solaire_Mobile_App__Solution_Architecture_Document_Phase2_v1.2.docx](/uploads/fb461da9d821363312cda6de6b667996/Solaire_Mobile_App__Solution_Architecture_Document_Phase2_v1.2.docx)

<b>Test cases - Phase 1:</b> [Solaire_Mobile_app_phase_1_Test_Cases.xlsx](/uploads/d5570908e259e3e2bfe6859a29af761a/Solaire_Mobile_app_phase_1_Test_Cases.xlsx)
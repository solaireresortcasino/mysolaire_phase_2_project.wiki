{
  "Header": {
    "TransactionID": "4735",
    "TimeStamp": "2017-08-07T15:16:17.0000335+08:00",
    "PrimaryLangID": "E",
    "OriginEntityID": "WEST",
    "OriginSystemType": "PMS",
    "DestinationEntityID": "OWM",
    "DestinationSystemType": "WEB"
  },
  "Body": {
    "HotelReservations": {
      "HotelReservation": [
        {
          "resultStatus": "SUCCESS",
          "UniqueIDList": {
            "UniqueID": [
              {
                "type": "INTERNAL",
                "ID": "657637"
              },
              {
                "type": "INTERNAL",
                "source": "RESVID",
                "ID": "627838"
              }
            ]
          },
          "marketSegment": "RR",
          "sourceCode": "OWM",
          "referralIYN": "N",
          "roomTypeCode": "DLKC",
          "numberOfUnits": "1",
          "roomTypeName": "Deluxe_Room||City_View||King_Bed",
          "roomTypeDescription": "<html>\n  <head>\n    \n  <\/head>\n  <body>\n    <p>\n      Enjoy luxurious accommodations in the most spacious deluxe rooms in the \n      country at 43 square meters, designed for ultimate comfort with a \n      stunning city view.||||ROOM FEATURES &amp; AMENITIES||1 king-size bed or 2 \n      double beds with 350-thread count linens||Floor-to-ceiling windows with \n      a panoramic view||47&#8221; flat LCD screen television with dedicated \n      international and local HDTV channels||Spacious work place||In-room \n      safe||Separate shower stall and bath tub||High speed internet \n      access||International Direct Dial Telephone and Voice Mail||In room \n      coffee and tea making facility||Private bar\n    <\/p>\n  <\/body>\n<\/html>\n",
          "roomTypeShortDescription": "Deluxe Room King City View",
          "RatePlans": {
            "RatePlan": [
              {
                "ratePlanCode": "HBAR",
                "suppressRate": "false",
                "ratePlanName": "Best Available Rate",
                "ratePlanDescription": "<html>\n  <head>\n    \n  <\/head>\n  <body>\n    <p>\n      Savor an unforgettable Solaire staycation at our Best Available \n      Rates.||Inclusions:||Unlimited WiFi access||Complimentary \n      self-parking||Complimentary local calls|| Access to the fitness center \n      and swimming pool||Terms and Conditions:|| Room reservations are subject \n      to availability. Blackout dates may apply.||Rate changes may apply \n      throughout the guest&#8217;s stay and restrictions may apply.||Rates are \n      subject to 10% service charge and 12% VAT ||Cancellations and revisions \n      are allowed 72 hours prior to arrival date. Late cancellations, \n      revisions and no shows are subject to a fee equivalent to one night&#8217;s \n      rate\n    <\/p>\n  <\/body>\n<\/html>\n",
                "ratePlanShortDescription": "Savor and unforgettable Solaire staycation at our Best Available Rates",
                "cancellationDateTime": "2017-08-04T12:00:00",
                "AdditionalDetails": [
                  {
                    "detailType": "CancelPolicy",
                    "additionalDetailDescription": "Cancel by 04-AUG-17"
                  },
                  {
                    "detailType": "TaxInformation",
                    "additionalDetailDescription": "Subject to applicable Government Tax and Room Service Charge Room VAT "
                  },
                  {
                    "detailType": "MarketingInformation",
                    "additionalDetailDescription": "Thank you for your booking"
                  },
                  {
                    "detailType": "DepositPolicy",
                    "additionalDetailDescription": "A deposit is not required for guarantee of your reservation"
                  }
                ]
              }
            ]
          },
          "RoomRates": {
            "RoomRate": [
              {
                "suppressRate": "false",
                "Rates": {
                  "Rate": [
                    {
                      "base": "9000",
                      "AdditionalGuestAmounts": {
                        "AdditionalGuestAmount": [
                          {
                            "amount": "0",
                            "additionalGuestType": "CHILD",
                            "quantity": "0"
                          },
                          {
                            "amount": "0",
                            "additionalGuestType": "ADULT",
                            "quantity": "0"
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          },
          "GuestCounts": {
            "isPerRoom": "true",
            "GuestCount": [
              {
                "ageQualifyingCode": "ADULT",
                "count": "2"
              },
              {
                "ageQualifyingCode": "CHILD",
                "count": "0"
              },
              {
                "ageQualifyingCode": "CHILDBUCKET1",
                "count": "0"
              },
              {
                "ageQualifyingCode": "CHILDBUCKET2",
                "count": "0"
              }
            ]
          },
          "TimeSpan": {
            "startDate": "2017-08-07T00:00:00",
            "endDate": "2017-08-08T00:00:00"
          },
          "Guarantee": {
            "guaranteeType": "MOB"
          },
          "Payment": {
            "PaymentsAccepted": {
              "PaymentType": [
                {
                  "OtherPayment": {
                    "type": "CASH"
                  }
                }
              ]
            }
          },
          "HotelReference": {
            "chainCode": "CHA",
            "hotelCode": "SRCM"
          },
          "Total": {
            "value": "10980"
          },
          "ExpectedCharges": {
            "totalRoomRateAndPackages": "9000",
            "taxInclusive": "false",
            "ChargesForPostingDate": [
              {
                "postingDate": "2017-08-07",
                "RoomRateAndPackages": {
                  "totalCharges": "9000",
                  "Charges": [
                    {
                      "description": "BASE RATE",
                      "amount": "9000"
                    }
                  ]
                },
                "TaxesAndFees": {
                  "totalCharges": "1980",
                  "Charges": [
                    {
                      "description": "80003 Room VAT",
                      "amount": "1080",
                      "codeType": "R",
                      "code": "10001"
                    },
                    {
                      "description": "80001 Room Service Charge",
                      "amount": "900",
                      "codeType": "R",
                      "code": "10001"
                    }
                  ]
                }
              }
            ]
          },
          "ResGuests": {
            "ResGuest": [
              {
                "resGuestRPH": "0",
                "Profiles": {
                  "Profile": [
                    {
                      "Customer": {
                        "gender": "MALE",
                        "personName": {
                          "nameTitle": "Mr.",
                          "firstName": "Test",
                          "lastName": "Test"
                        }
                      },
                      "Addresses": {
                        "NameAddress": [
                          {
                            "countryCode": "IN"
                          }
                        ]
                      },
                      "ProfileIDs": {
                        "UniqueID": [
                          {
                            "uniqueIDtype": "INTERNAL",
                            "ID": "344061"
                          }
                        ]
                      },
                      "Phones": {
                        "NamePhone": [
                          {
                            "phonetype": "HOME",
                            "phoneRole": "PHONE",
                            "primary": "true",
                            "phoneNumber": "23564788"
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          },
          "ReservationHistory": {
            "insertUser": "OEDS$OWS",
            "insertDate": "2017-08-07T15:15:41",
            "updateUser": "OEDS$OWS",
            "updateDate": "2017-08-07T15:15:41"
          }
        }
      ]
    },
    "Summary": {
      "pointsToBeEarned": "0",
      "totalAmount": "10980.0",
      "totalServiceCharges": "900.0",
      "totalVAT": "1080.0",
      "totalBaseRate": "9000.0",
      "Addons": {
        "totalAmount": "0",
        "Addon": [
          
        ]
      }
    }
  }
}
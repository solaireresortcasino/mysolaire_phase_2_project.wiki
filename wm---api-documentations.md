Sl	API ID	Integration Name

<b>Phase 2 APIs	</b>	
* 1	A001	GetConfigurations - 
* 2	A002	Get Member Info
* 3	A003	Search Room
* 4	A004	Search Package
* 5	A005	Detailed Availability
* 6	A006	GetAddons
* 7	A007	Create New Booking
* 8	A008	UpdateMPI Status
* 9	A009	CheckMPIStatus
* 10	A010	Payment Authorization
* 11	A011	RedeemPoints
* 12	A012	Get Booking Info
* 13	A013	Modify booking
* 14	A014	Cancel Booking
* 15	A015	Get CountriesList
* 16	A016	Get Currencies 
* 17	A017	UpdateRegistration
* 18	A018	ExpressCheckOut
* 19	A019	NewMemberSignup
* 20	A020	Get Cards 
* 21	A021	Update Cards
* 22	A022	GetEventList
* 23	A023	UpdateEventParticipation


<b>Phase 1 APIs	</b>			
* 24	A024	Authenticate
* 25	A025	GetPatronPromotions
* 26	A026	LogOff
* 27	A027	GetAccountDetail
* 28	A028	RedeemOffer
* 29	A029	ActivateDrawingEntry
* 30	A030	GetPatronPromotionsImages